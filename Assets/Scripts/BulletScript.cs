﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {

	public Rigidbody2D bullet;
	public bool bulletDead = false;
	public float bulletLife = 2;

	// Use this for initialization
	void Start () {
	bullet = GetComponent<Rigidbody2D> ();

	
	}
	
	// Update is called once per frame
	public float bulletForce = 1f;
	void Update () 
	{
		bulletLife--;
		bullet.AddForce (new Vector2 (-bulletForce, 0));

		if(bulletLife == 0)
			DestroyObject (gameObject);
		}

	}
	
