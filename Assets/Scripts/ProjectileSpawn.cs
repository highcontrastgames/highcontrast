﻿using UnityEngine;
using System.Collections;

public class ProjectileSpawn : MonoBehaviour {

	public GameObject projectile;
	public GameObject ProjectileSpawner;
	public float translation = 1f;


	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public void shoot()
	{
		Instantiate (projectile,ProjectileSpawner.transform.position, Quaternion.identity);
	}
}
