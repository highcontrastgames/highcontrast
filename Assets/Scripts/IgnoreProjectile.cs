﻿using UnityEngine;
using System.Collections;

public class IgnoreProjectile : MonoBehaviour {


	void OnCollisionEnter2D(Collision2D bullet){
		if (gameObject.tag == "Projectile") {
			Physics2D.IgnoreCollision (bullet.collider, GetComponent<Collider2D>());
		} 
	}
}
