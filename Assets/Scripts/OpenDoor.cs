﻿using UnityEngine;
using System.Collections;

public class OpenDoor : MonoBehaviour {

	public GameObject bullseye;
	public bool TargetHit;
	public GameObject Door;


	// Use this for initialization
	void Start () {
		


	}
	
	// Update is called once per frame
	void Update () {
		TargetHit = bullseye.GetComponent<TargetHit> ().TargetIsHit;
		if (TargetHit) {
			Destroy (Door);
		
		}
		
	}

}


