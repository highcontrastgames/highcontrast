﻿using UnityEngine;
using System.Collections;

public class controller : MonoBehaviour
{

    public float maxSpeed = 10f;
    public float Acceleration = 1f;
    public float objectSpeed = 1f;

    public Rigidbody2D rb;
    public float gravity;
    public float speed = 2f;
    public float jumpSpeed = 1f;

    bool grounded = false;
    public Transform groundCheck;
    float groundRadius = 0.2f;
    public LayerMask whatIsGround;

	public Camera WhiteWorldCam;
	public Camera BlackWorldCam;

	public GameObject WhiteWorld;
	public GameObject BlackWorld;

	public bool facingRight;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
		WhiteWorldCam.enabled = false;
		BlackWorldCam.enabled = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);



        if (Input.GetKey(KeyCode.LeftArrow))
        {
			BlackWorld.SetActive (true);
			WhiteWorld.SetActive (false);
            transform.Translate(Vector3.left * objectSpeed * Acceleration * Time.deltaTime);
            Physics2D.IgnoreLayerCollision(8, 10, false);
            Physics2D.IgnoreLayerCollision(8, 9, true);
			WhiteWorldCam.enabled = false;
			BlackWorldCam.enabled = true;
			facingRight = false;


        }

        if (Input.GetKey(KeyCode.DownArrow))
            transform.Translate(Vector2.down * objectSpeed * Acceleration * Time.deltaTime);

        if (Input.GetKey(KeyCode.RightArrow))
        {
			BlackWorld.SetActive (false);
			WhiteWorld.SetActive (true);
            transform.Translate(Vector3.right * objectSpeed * Acceleration * Time.deltaTime);
            Physics2D.IgnoreLayerCollision(8, 9, false);
            Physics2D.IgnoreLayerCollision(8, 10, true);
			WhiteWorldCam.enabled = true;
			BlackWorldCam.enabled = false;
			facingRight = true;

           
        }

    }
    void Update()
    {
        if (grounded && Input.GetKeyDown(KeyCode.UpArrow))
        {
            rb.AddForce(new Vector2(0, jumpSpeed));
        }
    }
}


//vector2 is vertical, vector3 is horizontal