﻿using UnityEngine;
using System.Collections;

public class AttackScript : MonoBehaviour {

	public GameObject projectile;
	public GameObject player;
	public float translation = 0;
	public GameObject HitCollider;

	// Use this for initialization
	void Start () 
	{
		HitCollider.SetActive (false);

	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButtonDown ("Fire1")) {

			Instantiate (projectile, player.transform.position*translation, Quaternion.identity);
		}
		if (Input.GetButtonDown ("Fire2")) {

			Debug.Log ("getting there");
			HitCollider.SetActive (true);

		}

		else 
			HitCollider.SetActive (false);
	}

	void FixedUpdate()
	{
		
	}
}
