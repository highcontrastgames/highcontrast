﻿using UnityEngine;
using System.Collections;

public class KillScreen : MonoBehaviour {
	
	public GameObject EndScreen;
	// Use this for initialization
	void Start () {
	
	}

	void OnTriggerEnter2D (Collider2D col){
		if (col.tag == "Player")
			EndScreen.SetActive (true);
	
	}
	
	// Update is called once per frame
	void Update () {

	
	}
}
